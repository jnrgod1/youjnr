const container = document.querySelector('#container');

document.querySelector('#btn-menu').addEventListener('click', () => {
    container.classList.toggle('active');
});

const comprove = () => {
    if(window.innerWidth <= 768) {
        container.classList.remove('active');
    }
    else {
        container.classList.add('active');
    }
}

comprove();

window.addEventListener('resize', () => {
    comprove();
});


